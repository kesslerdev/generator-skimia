/* eslint-disable no-await-in-loop */
import Generator from 'yeoman-generator'
import { join } from 'path'
import yeoman from 'yeoman-environment'
import exec from 'exec-sh'
import makePackages from './packages'

const { promise: execSh } = exec

const envRun = (env, args) =>
  new Promise((resolve) => {
    env.run(...args, () => resolve())
  })

export default class extends Generator {
  async prompting() {
    this.answers = await this.prompt([
      {
        type: 'confirm',
        name: 'mono',
        message: 'Mono Repo?',
      },
      {
        type: 'confirm',
        name: 'rootPrettier',
        message: 'Use root prettier?',
        when: ({ mono }) => mono,
      },
      {
        type: 'confirm',
        name: 'rootEslint',
        message: 'Use root eslint?',
        when: ({ mono }) => mono,
      },
      {
        type: 'list',
        name: 'rootEslintVersion',
        message: 'Eslint airbnb version?',
        choices: ['base', 'react'],
        when: ({ mono, rootEslint }) => mono && rootEslint,
      },
    ])
  }

  async configuring() {
    if (
      this.answers.mono
      && !this.fs.exists(this.destinationPath('package.json'))
    ) {
      await execSh('yarn init -p && yarn', {
        cwd: this.destinationRoot(),
      })
    }
  }

  async writing() {
    this.sourceRoot(join(__dirname, 'templates'))
    if (this.answers.mono) {
      let scripts = {
        start: 'oao run-script --parallel start',
        test: 'oao run-script test',
      }

      if (this.answers.rootEslint) {
        scripts = { ...scripts, pretest: 'eslint .' }
      }

      this.fs.writeJSON(this.destinationPath('package.json'), {
        ...this.fs.readJSON(this.destinationPath('package.json')),
        private: true,
        scripts,
        workspaces: {
          packages: [],
        },
      })
    }
    if (this.answers.rootPrettier) {
      this.fs.copy(
        this.templatePath('.prettierrc'),
        this.destinationPath('.prettierrc'),
      )
    }

    if (this.answers.rootEslint) {
      this.fs.copyTpl(
        this.templatePath('.eslintrc'),
        this.destinationPath('.eslintrc'),
        {
          type:
            this.answers.rootEslintVersion === 'base'
              ? 'airbnb-base'
              : 'airbnb',
        },
      )
      if (this.answers.rootEslintVersion === 'base') {
        this.fs.writeJSON(this.destinationPath('.eslintrc'), {
          ...this.fs.readJSON(this.destinationPath('.eslintrc')),
          env: {
            jest: true,
            node: true,
          },
        })
      }
      this.fs.copy(
        this.templatePath('.eslintignore'),
        this.destinationPath('.eslintignore'),
      )
    }
  }

  async install() {
    if (this.answers.mono) {
      this.yarnInstall(['oao'], {
        dev: true,
        'ignore-workspace-root-check': true,
      })
    }

    if (this.answers.rootEslint) {
      const distrib = this.answers.rootEslintVersion === 'base'
        ? 'eslint-config-airbnb-base'
        : 'eslint-config-airbnb'

      await execSh(
        `npx install-peerdeps ${distrib} --dev -Y --extra-args '-W'`,
        {
          cwd: this.destinationRoot(),
        },
      )
    }
  }

  async end() {
    const env = yeoman.createEnv()
    env.lookup(async () => {
      if (!this.answers.mono) {
        env.run('skimia:package')
      } else {
        let another = false
        const paths = []
        do {
          const { path } = await this.prompt({
            type: 'input',
            name: 'path',
            message: 'Package path within workspace',
          })
          paths.push(path)
          const fullPath = join(this.destinationRoot(), path)
          await execSh(`mkdir -p ${fullPath}`, {
            cwd: this.destinationRoot(),
          })
          await envRun(env, ['skimia:package', { destinationRoot: fullPath }]);
          ({ another } = await this.prompt({
            type: 'confirm',
            name: 'another',
            message: 'Create another package?',
          }))
        } while (another)

        const packages = makePackages(paths)

        const original = this.fs.readJSON(this.destinationPath('package.json'))
        this.fs.writeJSON(this.destinationPath('package.json'), {
          ...original,
          workspaces: {
            ...original.workspaces,
            packages,
          },
        })
      }
    })
  }
}
