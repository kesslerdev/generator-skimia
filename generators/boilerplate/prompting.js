"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _glob = _interopRequireDefault(require("glob"));

var _path = require("path");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// TODO: Implement find global packages using keywords
const findBoilerplates = () => new Promise(resolve => {
  (0, _glob.default)('boilerplate-*/', {
    cwd: (0, _path.dirname)(__dirname)
  }, (er, files) => {
    resolve(files.map(d => d.replace(/\/|boilerplate-/g, '')));
  });
});

const prompting = async thisArg => {
  if (!thisArg.options.prompt) {
    const config = thisArg.config.getAll();

    if (!config.promptValues) {
      throw new Error('Cannot run the first time with non interactive option');
    }

    return _objectSpread({}, thisArg.props, config.promptValues);
  }

  const boilerplateProps = await thisArg.prompt([{
    type: 'list',
    message: 'Select what boilerplate yout want',
    name: 'boilerplate',
    choices: ['none', ...(await findBoilerplates())],
    filter: boilerplate => boilerplate === 'none' ? null : `skimia:boilerplate-${boilerplate}`,
    store: true
  }]);
  return _objectSpread({}, thisArg.props, boilerplateProps);
};

var _default = prompting;
exports.default = _default;