import { Separator } from 'inquirer'
import askName from 'inquirer-npm-name'
import { basename } from 'path'

const prompting = async (thisArg) => {
  if (!thisArg.options.prompt) {
    const config = thisArg.config.getAll()

    if (!config.promptValues) {
      throw new Error('Cannot run the first time with non interactive option')
    }

    return {
      ...thisArg.props,
      ...config.promptValues,
    }
  }

  const nameProps = await askName(
    {
      name: 'name',
      message: 'Your package name',
      default: basename(process.cwd()),
      store: true,
    },
    thisArg,
  )

  const projectProps = await thisArg.prompt([
    {
      type: 'list',
      name: 'type',
      message: 'One last thing, what is the type of project ?',
      choices: ['lib', 'app'],
      store: true,
    },
  ])

  let choices = [
    new Separator(' = Node Cool = '),
    {
      name: 'eslint',
      checked: true,
    },
    {
      name: 'prettier',
      checked: true,
    },
    {
      name: 'babel',
      checked: true,
    },
    {
      name: 'flow',
      checked: true,
    },
    new Separator(' = Testing = '),
    {
      name: 'jest',
      checked: true,
    },
    new Separator(' = CI ='),
    {
      name: 'gitlab-ci',
    },
    {
      name: 'travis',
      disabled: 'WIP',
    },
  ]

  if (projectProps.type === 'app') {
    choices = [
      ...choices,
      new Separator(' = DEPLOY ='),
      {
        name: 'docker',
      },
    ]
  }
  const configProps = await thisArg.prompt([
    {
      type: 'checkbox',
      message: 'Select what yout want',
      name: 'features',
      choices,
      validate(answer) {
        if (answer.length < 1) {
          return 'You must choose at least one thing.'
        }
        return true
      },
      store: true,
    },
  ])

  return {
    ...thisArg.props,
    ...nameProps,
    ...projectProps,
    ...configProps,
    ...{ features: [...configProps.features, 'default'] },
  }
}

export default prompting
