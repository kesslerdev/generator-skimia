"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _path = require("path");

var _default = pkgs => [...new Set(pkgs.map(pkg => {
  const parts = pkg.split(_path.sep);

  if (parts.length === 1) {
    return pkg;
  }

  parts.pop();
  return (0, _path.join)(...parts, '*');
}))];

exports.default = _default;