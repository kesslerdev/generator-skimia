"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _yeomanGenerator = _interopRequireDefault(require("yeoman-generator"));

var _path = require("path");

var _yeomanEnvironment = _interopRequireDefault(require("yeoman-environment"));

var _execSh = _interopRequireDefault(require("exec-sh"));

var _packages = _interopRequireDefault(require("./packages"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const {
  promise: execSh
} = _execSh.default;

const envRun = (env, args) => new Promise(resolve => {
  env.run(...args, () => resolve());
});

class _default extends _yeomanGenerator.default {
  async prompting() {
    this.answers = await this.prompt([{
      type: 'confirm',
      name: 'mono',
      message: 'Mono Repo?'
    }, {
      type: 'confirm',
      name: 'rootPrettier',
      message: 'Use root prettier?',
      when: ({
        mono
      }) => mono
    }, {
      type: 'confirm',
      name: 'rootEslint',
      message: 'Use root eslint?',
      when: ({
        mono
      }) => mono
    }, {
      type: 'list',
      name: 'rootEslintVersion',
      message: 'Eslint airbnb version?',
      choices: ['base', 'react'],
      when: ({
        mono,
        rootEslint
      }) => mono && rootEslint
    }]);
  }

  async configuring() {
    if (this.answers.mono && !this.fs.exists(this.destinationPath('package.json'))) {
      await execSh('yarn init -p && yarn', {
        cwd: this.destinationRoot()
      });
    }
  }

  async writing() {
    this.sourceRoot((0, _path.join)(__dirname, 'templates'));

    if (this.answers.mono) {
      this.fs.writeJSON(this.destinationPath('package.json'), _objectSpread({}, this.fs.readJSON(this.destinationPath('package.json')), {
        private: true,
        scripts: {
          start: 'oao run-script --parallel start',
          pretest: 'eslint .',
          test: 'oao run-script test'
        },
        workspaces: {
          packages: []
        }
      }));
    }

    if (this.answers.rootPrettier) {
      this.fs.copy(this.templatePath('.prettierrc'), this.destinationPath('.prettierrc'));
    }

    if (this.answers.rootEslint) {
      this.fs.copyTpl(this.templatePath('.eslintrc'), this.destinationPath('.eslintrc'), {
        type: this.answers.rootEslintVersion === 'base' ? 'airbnb-base' : 'airbnb'
      });

      if (this.answers.rootEslintVersion === 'base') {
        this.fs.writeJSON(this.destinationPath('.eslintrc'), _objectSpread({}, this.fs.readJSON(this.destinationPath('.eslintrc')), {
          env: {
            jest: true,
            node: true
          }
        }));
      }

      this.fs.copy(this.templatePath('.eslintignore'), this.destinationPath('.eslintignore'));
    }
  }

  async install() {
    if (this.answers.mono) {
      this.yarnInstall(['oao'], {
        dev: true,
        'ignore-workspace-root-check': true
      });
    }

    if (this.answers.rootEslint) {
      const distrib = this.answers.rootEslintVersion === 'base' ? 'eslint-config-airbnb-base' : 'eslint-config-airbnb';
      await execSh(`npx install-peerdeps ${distrib} --dev -Y --extra-args '-W'`, {
        cwd: this.destinationRoot()
      });
    }
  }

  async end() {
    const env = _yeomanEnvironment.default.createEnv();

    env.lookup(async () => {
      if (!this.answers.mono) {
        env.run('skimia:package');
      } else {
        let another = false;
        const paths = [];

        do {
          const {
            path
          } = await this.prompt({
            type: 'input',
            name: 'path',
            message: 'Package path within workspace'
          });
          paths.push(path);
          const fullPath = (0, _path.join)(this.destinationRoot(), path);
          await execSh(`mkdir -p ${fullPath}`, {
            cwd: this.destinationRoot()
          });
          await envRun(env, ['skimia:package', {
            destinationRoot: fullPath
          }]);
          ({
            another
          } = await this.prompt({
            type: 'confirm',
            name: 'another',
            message: 'Create another package?'
          }));
        } while (another);

        const packages = (0, _packages.default)(paths);
        const original = this.fs.readJSON(this.destinationPath('package.json'));
        this.fs.writeJSON(this.destinationPath('package.json'), _objectSpread({}, original, {
          workspaces: _objectSpread({}, original.workspaces, {
            packages
          })
        }));
      }
    });
  }

}

exports.default = _default;