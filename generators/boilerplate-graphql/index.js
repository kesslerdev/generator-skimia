"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _yeomanGenerator = _interopRequireDefault(require("yeoman-generator"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

class _default extends _yeomanGenerator.default {
  async prompting() {
    this.log('sapass prompting graphQL');
  }

  default() {
    this.log('sapass default graphQL');
  }

}

exports.default = _default;