"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _yeomanGenerator = _interopRequireDefault(require("yeoman-generator"));

var _prompting = _interopRequireDefault(require("./prompting"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

class _default extends _yeomanGenerator.default {
  constructor(args, opts) {
    super(args, opts);
    this.option('prompt', {
      type: Boolean,
      default: true
    });
  }

  async prompting() {
    this.props = await (0, _prompting.default)(this);

    if (this.props.boilerplate) {
      this.composeWith(this.props.boilerplate, {
        prompt: this.options.prompt
      });
    }
  }

}

exports.default = _default;