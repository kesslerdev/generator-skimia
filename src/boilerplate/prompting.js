import glob from 'glob'
import { dirname } from 'path'

// TODO: Implement find global packages using keywords
const findBoilerplates = () =>
  new Promise((resolve) => {
    glob('boilerplate-*/', { cwd: dirname(__dirname) }, (er, files) => {
      resolve(files.map(d => d.replace(/\/|boilerplate-/g, '')))
    })
  })

const prompting = async (thisArg) => {
  if (!thisArg.options.prompt) {
    const config = thisArg.config.getAll()

    if (!config.promptValues) {
      throw new Error('Cannot run the first time with non interactive option')
    }

    return {
      ...thisArg.props,
      ...config.promptValues,
    }
  }
  const boilerplateProps = await thisArg.prompt([
    {
      type: 'list',
      message: 'Select what boilerplate yout want',
      name: 'boilerplate',
      choices: ['none', ...(await findBoilerplates())],
      filter: boilerplate =>
        (boilerplate === 'none' ? null : `skimia:boilerplate-${boilerplate}`),
      store: true,
    },
  ])

  return {
    ...thisArg.props,
    ...boilerplateProps,
  }
}

export default prompting
