"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.mergeFiles = exports.mergeScripts = exports.mergeDeps = void 0;

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const mergeScripts = (type, enabledFeatures, features) => {
  let scripts = {};
  enabledFeatures.forEach(feature => {
    var _features$feature, _features$feature$scr, _features$feature2, _features$feature2$sc;

    scripts = _objectSpread({}, scripts, (_features$feature = features[feature]) === null || _features$feature === void 0 ? void 0 : (_features$feature$scr = _features$feature.scripts) === null || _features$feature$scr === void 0 ? void 0 : _features$feature$scr.all, (_features$feature2 = features[feature]) === null || _features$feature2 === void 0 ? void 0 : (_features$feature2$sc = _features$feature2.scripts) === null || _features$feature2$sc === void 0 ? void 0 : _features$feature2$sc[type]);
    enabledFeatures.forEach(enabled => {
      var _features$feature3, _features$feature3$sc, _features$feature3$sc2;

      scripts = _objectSpread({}, scripts, (_features$feature3 = features[feature]) === null || _features$feature3 === void 0 ? void 0 : (_features$feature3$sc = _features$feature3.scripts) === null || _features$feature3$sc === void 0 ? void 0 : (_features$feature3$sc2 = _features$feature3$sc.features) === null || _features$feature3$sc2 === void 0 ? void 0 : _features$feature3$sc2[enabled]);
    });
  });
  return scripts;
};

exports.mergeScripts = mergeScripts;

const mergeDeps = (type, enabledFeatures, features) => {
  let deps = [];
  enabledFeatures.forEach(feature => {
    var _features$feature4, _features$feature4$de, _features$feature5, _features$feature5$de;

    deps = [...deps, ...(((_features$feature4 = features[feature]) === null || _features$feature4 === void 0 ? void 0 : (_features$feature4$de = _features$feature4.deps) === null || _features$feature4$de === void 0 ? void 0 : _features$feature4$de.all) || []), ...(((_features$feature5 = features[feature]) === null || _features$feature5 === void 0 ? void 0 : (_features$feature5$de = _features$feature5.deps) === null || _features$feature5$de === void 0 ? void 0 : _features$feature5$de[type]) || [])];
    enabledFeatures.forEach(enabled => {
      var _features$feature6, _features$feature6$de, _features$feature6$de2;

      deps = [...deps, ...(((_features$feature6 = features[feature]) === null || _features$feature6 === void 0 ? void 0 : (_features$feature6$de = _features$feature6.deps) === null || _features$feature6$de === void 0 ? void 0 : (_features$feature6$de2 = _features$feature6$de.features) === null || _features$feature6$de2 === void 0 ? void 0 : _features$feature6$de2[enabled]) || [])];
    });
  });
  return deps;
};

exports.mergeDeps = mergeDeps;

const mergeFiles = (type, enabledFeatures, features) => {
  let files = [];
  enabledFeatures.forEach(feature => {
    var _features$feature7, _features$feature7$fi, _features$feature8, _features$feature8$fi;

    files = [...files, ...(((_features$feature7 = features[feature]) === null || _features$feature7 === void 0 ? void 0 : (_features$feature7$fi = _features$feature7.files) === null || _features$feature7$fi === void 0 ? void 0 : _features$feature7$fi.all) || []), ...(((_features$feature8 = features[feature]) === null || _features$feature8 === void 0 ? void 0 : (_features$feature8$fi = _features$feature8.files) === null || _features$feature8$fi === void 0 ? void 0 : _features$feature8$fi[type]) || [])];
    enabledFeatures.forEach(enabled => {
      var _features$feature9, _features$feature9$fi, _features$feature9$fi2;

      files = [...files, ...(((_features$feature9 = features[feature]) === null || _features$feature9 === void 0 ? void 0 : (_features$feature9$fi = _features$feature9.files) === null || _features$feature9$fi === void 0 ? void 0 : (_features$feature9$fi2 = _features$feature9$fi.features) === null || _features$feature9$fi2 === void 0 ? void 0 : _features$feature9$fi2[enabled]) || [])];
    });
  });
  return files;
};

exports.mergeFiles = mergeFiles;