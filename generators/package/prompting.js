"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _inquirer = require("inquirer");

var _inquirerNpmName = _interopRequireDefault(require("inquirer-npm-name"));

var _path = require("path");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const prompting = async thisArg => {
  if (!thisArg.options.prompt) {
    const config = thisArg.config.getAll();

    if (!config.promptValues) {
      throw new Error('Cannot run the first time with non interactive option');
    }

    return _objectSpread({}, thisArg.props, config.promptValues);
  }

  const nameProps = await (0, _inquirerNpmName.default)({
    name: 'name',
    message: 'Your package name',
    default: (0, _path.basename)(process.cwd()),
    store: true
  }, thisArg);
  const projectProps = await thisArg.prompt([{
    type: 'list',
    name: 'type',
    message: 'One last thing, what is the type of project ?',
    choices: ['lib', 'app'],
    store: true
  }]);
  let choices = [new _inquirer.Separator(' = Node Cool = '), {
    name: 'eslint',
    checked: true
  }, {
    name: 'prettier',
    checked: true
  }, {
    name: 'babel',
    checked: true
  }, {
    name: 'flow',
    checked: true
  }, new _inquirer.Separator(' = Testing = '), {
    name: 'jest',
    checked: true
  }, new _inquirer.Separator(' = CI ='), {
    name: 'gitlab-ci'
  }, {
    name: 'travis',
    disabled: 'WIP'
  }];

  if (projectProps.type === 'app') {
    choices = [...choices, new _inquirer.Separator(' = DEPLOY ='), {
      name: 'docker'
    }];
  }

  const configProps = await thisArg.prompt([{
    type: 'checkbox',
    message: 'Select what yout want',
    name: 'features',
    choices,

    validate(answer) {
      if (answer.length < 1) {
        return 'You must choose at least one thing.';
      }

      return true;
    },

    store: true
  }]);
  return _objectSpread({}, thisArg.props, nameProps, projectProps, configProps, {
    features: [...configProps.features, 'default']
  });
};

var _default = prompting;
exports.default = _default;