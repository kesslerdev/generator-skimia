import Generator from 'yeoman-generator'

export default class extends Generator {
  async prompting() {
    this.log('sapass prompting graphQL')
  }

  default() {
    this.log('sapass default graphQL')
  }
}
