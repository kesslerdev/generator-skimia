"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _yeomanGenerator = _interopRequireDefault(require("yeoman-generator"));

var _features = _interopRequireDefault(require("./features"));

var _merge = require("./merge");

var _prompting = _interopRequireDefault(require("./prompting"));

var _default2 = _interopRequireDefault(require("./default"));

var _writing = _interopRequireDefault(require("./writing"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

class _default extends _yeomanGenerator.default {
  constructor(args, opts) {
    super(args, opts);
    this.option('prompt', {
      type: Boolean,
      default: true
    });

    if (opts.destinationRoot) {
      this.destinationRoot(opts.destinationRoot);
      this.disableAutoCreate = true;
    }
  }

  initializing() {
    this.props = {};
    this.composeWith('skimia:boilerplate', {
      prompt: this.options.prompt
    });
  }

  async prompting() {
    this.props = await (0, _prompting.default)(this);
    /* this.config.set('props', this.props) */

    this.config.save();
  }

  async default() {
    await (0, _default2.default)(this);
  }

  async writing() {
    await (0, _writing.default)(this);
  }

  install() {
    this.yarnInstall((0, _merge.mergeDeps)(this.props.type, this.props.features, _features.default), {
      dev: true
    });
  }

}

exports.default = _default;