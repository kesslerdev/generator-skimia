import Generator from 'yeoman-generator'
import features from './features'
import { mergeDeps } from './merge'

import prompt from './prompting'
import def from './default'
import writing from './writing'

export default class extends Generator {
  constructor(args, opts) {
    super(args, opts)

    this.option('prompt', {
      type: Boolean,
      default: true,
    })

    if (opts.destinationRoot) {
      this.destinationRoot(opts.destinationRoot)
      this.disableAutoCreate = true
    }
  }

  initializing() {
    this.props = {}

    this.composeWith('skimia:boilerplate', {
      prompt: this.options.prompt,
    })
  }

  async prompting() {
    this.props = await prompt(this)
    /* this.config.set('props', this.props) */
    this.config.save()
  }

  async default() {
    await def(this)
  }

  async writing() {
    await writing(this)
  }

  install() {
    this.yarnInstall(
      mergeDeps(this.props.type, this.props.features, features),
      { dev: true },
    )
  }
}
