import { sep, join } from 'path'

export default pkgs => [
  ...new Set(
    pkgs.map((pkg) => {
      const parts = pkg.split(sep)
      if (parts.length === 1) {
        return pkg
      }
      parts.pop()
      return join(...parts, '*')
    }),
  ),
]
