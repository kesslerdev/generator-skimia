import { isString } from 'lodash'

const features = {
  eslint: {
    files: {
      all: [
        {
          from: 'templates/.eslintrc.json',
          to: '.eslintrc.json',
        },
      ],
      features: {
        jest: [
          {
            // INFO: take care in transform operation
            // from & to are within destination
            from: '.eslintrc.json',
            to: '.eslintrc.json',
            transformJSON: json => ({
              ...json,
              plugins: [...(json.plugins || []), 'jest'],
              extends: [
                ...((isString(json.extends) ? [json.extends] : json.extends)
                  || []),
                'plugin:jest/recommended',
              ],
              env: { ...json.env, jest: true },
            }),
          },
        ],
        babel: [
          {
            from: '.eslintrc.json',
            to: '.eslintrc.json',
            transformJSON: json => ({
              ...json,
              parser: 'babel-eslint',
            }),
          },
        ],
      },
    },
    deps: {
      all: [
        'eslint@^5.3.0',
        'eslint-config-airbnb-base@^13.1.0',
        'eslint-plugin-import@^2.14.0',
      ],
      features: {
        jest: ['eslint-plugin-jest@^22.1.2'],
        babel: ['babel-eslint@^8.2.5'],
      },
    },
    scripts: {
      all: {
        pretest: 'eslint ./src',
      },
      features: {
        flow: {
          pretest: 'eslint ./src && flow check',
        },
      },
    },
  },
  babel: {
    files: {
      all: [
        {
          from: '.babelrc',
          to: '.babelrc',
        },
      ],
    },
    deps: {
      all: [
        '@babel/cli@^7.0.0',
        '@babel/core@^7.0.0',
        '@babel/plugin-proposal-object-rest-spread@^7.0.0',
        '@babel/plugin-proposal-optional-chaining@^7.0.0',
        '@babel/plugin-proposal-class-properties@^7.1.0',
        '@babel/preset-env@^7.0.0',
        '@babel/preset-flow@^7.0.0',
      ],
      app: ['@babel/node@^7.0.0'],
    },
    scripts: {
      lib: {
        watch: 'babel -w src -d dist',
        build: 'babel src -d dist',
      },
      app: {
        start: 'nodemon --delay 1 -w src --exec babel-node -- ./src/index.js',
        build: 'babel src -d dist',
      },
    },
  },
  flow: {
    files: {
      all: [
        {
          from: '.flowconfig',
          to: '.flowconfig',
        },
      ],
    },
    deps: {
      all: ['flow-bin@^0.75.0'],
    },
  },
  prettier: {
    files: {
      all: [
        {
          from: '.prettierrc',
          to: '.prettierrc',
        },
      ],
    },
  },
  jest: {
    files: {
      app: [
        {
          from: 'templates/test.app.js',
          to: 'tests/index.test.js',
          skipExisting: true,
          skipNoPrompt: true,
        },
      ],
      lib: [
        {
          from: 'templates/test.lib.js',
          to: 'tests/index.test.js',
          skipExisting: true,
          skipNoPrompt: true,
        },
      ],
    },
    deps: {
      all: ['jest@^23.2.0'],
      features: {
        babel: ['babel-jest@^23.2.0', 'babel-core@^7.0.0-0'],
      },
    },
    scripts: {
      all: {
        test: 'jest --coverage',
        watchtest: 'jest --coverage --watchAll',
      },
    },
  },
  'gitlab-ci': {
    files: {
      all: [
        {
          from: '.gitlab-ci.yml',
          to: '.gitlab-ci.yml',
        },
      ],
    },
  },
  docker: {
    files: {
      app: [
        {
          from: 'templates/Dockerfile',
          to: 'Dockerfile',
          skipExisting: true,
          skipNoPrompt: true,
        },
        {
          from: 'templates/.dockerignore',
          to: '.dockerignore',
          skipExisting: true,
          skipNoPrompt: true,
        },
      ],
    },
    scripts: {
      app: {
        'build-image': 'docker build -t {IMAGE} .',
      },
    },
  },
  default: {
    files: {
      app: [
        {
          from: 'templates/index.app.js',
          to: 'src/index.js',
          skipExisting: true,
          skipNoPrompt: true,
        },
      ],
      lib: [
        {
          from: 'templates/index.lib.js',
          to: 'src/index.js',
          skipExisting: true,
          skipNoPrompt: true,
        },
      ],
    },
    deps: {
      app: ['nodemon'],
    },
  },
}

export default features
