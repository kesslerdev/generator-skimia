import yoHelpers from 'yeoman-test'
import assert from 'yeoman-assert'
import { join } from 'path'
import features from '../generators/package/features'

const deps = [[yoHelpers.createDummyGenerator(), 'skimia:boilerplate']]

const makeApp = (added = [], type = 'app') =>
  yoHelpers
    .run(join(__dirname, '../generators/package'))
    .withGenerators(deps)
    .inDir(join(__dirname, './temp-app'))
    .withPrompts({
      name: 'temp-app',
      features: ['default', ...added],
      type,
    }) // Mock the prompt answers

describe('App generator', () => {
  test('package creation', () =>
    makeApp([]).then(() => {
      assert.file(['package.json', 'src/index.js'])

      assert.JSONFileContent('package.json', {
        name: 'temp-app',
        private: true,
      })
    }))

  test('eslint ', () =>
    makeApp(['eslint']).then(() => {
      assert.file(['.eslintrc.json'])

      assert.JSONFileContent('package.json', {
        scripts: {
          pretest: features.eslint.scripts.all.pretest,
        },
      })
    }))
})
