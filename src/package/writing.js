import { mergeScripts, mergeFiles } from './merge'
import features from './features'

const writing = async (thisArg) => {
  const scripts = mergeScripts(
    thisArg.props.type,
    thisArg.props.features,
    features,
  )
  const files = mergeFiles(thisArg.props.type, thisArg.props.features, features)

  files.forEach((file) => {
    if (
      (!file.skipExisting
        || !thisArg.fs.exists(thisArg.destinationPath(file.to)))
      && !(file.skipNoPrompt && !thisArg.options.prompt)
    ) {
      if (file.transformJSON) {
        thisArg.fs.writeJSON(
          thisArg.destinationPath(file.to),
          file.transformJSON(
            thisArg.fs.readJSON(thisArg.destinationPath(file.from)),
          ),
        )
      } else {
        thisArg.fs.copy(
          thisArg.templatePath(file.from),
          thisArg.destinationPath(file.to),
        )
      }
    }
  })

  let pkg = thisArg.fs.readJSON(thisArg.destinationPath('package.json'))
  pkg = {
    ...pkg,
    ...{
      scripts: {
        ...pkg.scripts,
        ...scripts,
      },
    },
  }
  thisArg.fs.writeJSON(thisArg.destinationPath('package.json'), pkg)
}

export default writing
