import mkdirp from 'mkdirp'
import { basename, dirname } from 'path'

const def = async (thisArg) => {
  if (
    basename(thisArg.destinationPath()) !== thisArg.props.name
    && !thisArg.disableAutoCreate
  ) {
    thisArg.log(
      `Your generator must be inside a folder named ${
        thisArg.props.name
      }\nI can automatically create this folder.`,
    )
    const { create } = await thisArg.prompt([
      {
        type: 'confirm',
        name: 'create',
        message: `Create directory ${thisArg.destinationPath(
          thisArg.props.name,
        )} ?`,
      },
    ])

    if (create) {
      mkdirp(thisArg.props.name)
      thisArg.destinationRoot(thisArg.destinationPath(thisArg.props.name))
    }
  }

  thisArg.sourceRoot(dirname(dirname(__dirname)))

  if (!thisArg.fs.exists(thisArg.destinationPath('package.json'))) {
    thisArg.log('First create package.json')
    thisArg.fs.writeJSON(thisArg.destinationPath('package.json'), {
      name: thisArg.props.name,
      version: '0.0.0',
      private: true,
      main: './dist/index.js',
    })
  }
}

export default def
