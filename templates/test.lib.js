import plus from '../src'

describe('Method #1', () => {
  test('Behavior #1', () => {
    expect(plus(1, 1)).toBe(2)
  })
})
