"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _mkdirp = _interopRequireDefault(require("mkdirp"));

var _path = require("path");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const def = async thisArg => {
  if ((0, _path.basename)(thisArg.destinationPath()) !== thisArg.props.name && !thisArg.disableAutoCreate) {
    thisArg.log(`Your generator must be inside a folder named ${thisArg.props.name}\nI can automatically create this folder.`);
    const {
      create
    } = await thisArg.prompt([{
      type: 'confirm',
      name: 'create',
      message: `Create directory ${thisArg.destinationPath(thisArg.props.name)} ?`
    }]);

    if (create) {
      (0, _mkdirp.default)(thisArg.props.name);
      thisArg.destinationRoot(thisArg.destinationPath(thisArg.props.name));
    }
  }

  thisArg.sourceRoot((0, _path.dirname)((0, _path.dirname)(__dirname)));

  if (!thisArg.fs.exists(thisArg.destinationPath('package.json'))) {
    thisArg.log('First create package.json');
    thisArg.fs.writeJSON(thisArg.destinationPath('package.json'), {
      name: thisArg.props.name,
      version: '0.0.0',
      private: true,
      main: './dist/index.js'
    });
  }
};

var _default = def;
exports.default = _default;