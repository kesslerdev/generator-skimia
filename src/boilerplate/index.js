import Generator from 'yeoman-generator'

import prompt from './prompting'

export default class extends Generator {
  constructor(args, opts) {
    super(args, opts)

    this.option('prompt', {
      type: Boolean,
      default: true,
    })
  }

  async prompting() {
    this.props = await prompt(this)
    if (this.props.boilerplate) {
      this.composeWith(this.props.boilerplate, {
        prompt: this.options.prompt,
      })
    }
  }
}
