"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _merge = require("./merge");

var _features = _interopRequireDefault(require("./features"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const writing = async thisArg => {
  const scripts = (0, _merge.mergeScripts)(thisArg.props.type, thisArg.props.features, _features.default);
  const files = (0, _merge.mergeFiles)(thisArg.props.type, thisArg.props.features, _features.default);
  files.forEach(file => {
    if ((!file.skipExisting || !thisArg.fs.exists(thisArg.destinationPath(file.to))) && !(file.skipNoPrompt && !thisArg.options.prompt)) {
      if (file.transformJSON) {
        thisArg.fs.writeJSON(thisArg.destinationPath(file.to), file.transformJSON(thisArg.fs.readJSON(thisArg.destinationPath(file.from))));
      } else {
        thisArg.fs.copy(thisArg.templatePath(file.from), thisArg.destinationPath(file.to));
      }
    }
  });
  let pkg = thisArg.fs.readJSON(thisArg.destinationPath('package.json'));
  pkg = _objectSpread({}, pkg, {
    scripts: _objectSpread({}, pkg.scripts, scripts)
  });
  thisArg.fs.writeJSON(thisArg.destinationPath('package.json'), pkg);
};

var _default = writing;
exports.default = _default;