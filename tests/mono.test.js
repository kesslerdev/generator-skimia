import makePackages from '../src/app/packages'

describe('App generator', () => {
  test('package creation', () => {
    const packages = ['frontend', 'backend/gateway', 'libs/jes/a', 'libs/jes/b']
    expect(makePackages(packages)).toEqual([
      'frontend',
      'backend/*',
      'libs/jes/*',
    ])
  })
})
