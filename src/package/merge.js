const mergeScripts = (type, enabledFeatures, features) => {
  let scripts = {}
  enabledFeatures.forEach((feature) => {
    scripts = {
      ...scripts,
      ...features[feature]?.scripts?.all,
      ...features[feature]?.scripts?.[type],
    }

    enabledFeatures.forEach((enabled) => {
      scripts = {
        ...scripts,
        ...features[feature]?.scripts?.features?.[enabled],
      }
    })
  })

  return scripts
}

const mergeDeps = (type, enabledFeatures, features) => {
  let deps = []
  enabledFeatures.forEach((feature) => {
    deps = [
      ...deps,
      ...(features[feature]?.deps?.all || []),
      ...(features[feature]?.deps?.[type] || []),
    ]

    enabledFeatures.forEach((enabled) => {
      deps = [...deps, ...(features[feature]?.deps?.features?.[enabled] || [])]
    })
  })

  return deps
}

const mergeFiles = (type, enabledFeatures, features) => {
  let files = []

  enabledFeatures.forEach((feature) => {
    files = [
      ...files,
      ...(features[feature]?.files?.all || []),
      ...(features[feature]?.files?.[type] || []),
    ]

    enabledFeatures.forEach((enabled) => {
      files = [
        ...files,
        ...(features[feature]?.files?.features?.[enabled] || []),
      ]
    })
  })

  return files
}
export { mergeDeps, mergeScripts, mergeFiles }
